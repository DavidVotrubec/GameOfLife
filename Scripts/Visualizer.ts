﻿
interface IVisualizer {
    toString(IWorld): string;
}

class Visualizer implements IVisualizer {
    toString(world: IWorld) {
        // TODO: Should be tested with mocked world

        var dimensions = world.getDimensions();
        var creatures = world.getCreatures();
        var x = dimensions[0];
        var y = dimensions[1];

        var allCells = [];
        
        for (let i = 0; i < x; i++) {
            for (let j = 0; j < y; j++) {
                allCells.push([i, j]);
            }
        }

        allCells.forEach((c, index) => {
            var isLiving = world.alreadyExists(c);
            if (isLiving) {
                allCells[index] = "X";
            }
            else {
                allCells[index] = " ";
            }
        });
        
        // split array into an array of array (per lines)
        var resultLines = []; //array of arrays
        for (let i = 0; i < x; i++) {
            
            var row = allCells.slice(0, x);
            resultLines.push(row);

            allCells = allCells.slice(x);
        }

        var result = '';
        resultLines.forEach(row => {
            result += row.toString() + '\n';
        });

        //return allCells.toString();
        return result;
    }
}

module.exports = Visualizer;