﻿(function () {
    var World = require('./World');

    describe("World", () => {
        it("should create world with N creatures", () => {
            var newWorld: IWorld = new World(2, 3);
            var creatures = newWorld.getCreatures();
            expect(creatures.length).toBe(2);
        });

        it("should create unique creatures", () => {
            var newWorld: IWorld = new World(2, 3);
            var creatures = newWorld.getCreatures();
            
            // TODO
        });
    })

})();