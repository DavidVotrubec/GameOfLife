﻿(function () {
    var Visualizer = require('./Visualizer');
    var World = require('./World');

    describe("Visualizer", () => {
        //it("should return world as string", () => {
        //    var newWorld = <IWorld>{
        //        getDimensions: function () {
        //            return [5, 3];
        //        }
        //        getCreatures: function () {
        //            return [
        //                [2, 0],
        //                [2, 1],
        //                [3, 1],
        //                [3, 2]
        //            ];
        //        }
        //    };
            
        //    newWorld.creatures = newWorld.getCreatures();
        //    newWorld.alreadyExists = () => World.prototype.alreadyExists.call(newWorld, newWorld.getCreatures());

        //    var visualizer: IVisualizer = new Visualizer();
        //    expect(visualizer.toString(newWorld)).toBe(' , ,X, , ,\n , ,X,X, ,\n , , ,X, ,\n , , , , ,\n , , , , ,\n');
        //});

        it("should return world as string", () => {
            var newWorld = <IWorld>{
                getDimensions: function () {
                    return [1, 1];
                }
                getCreatures: function () {
                    return [
                        [0, 0]
                    ];
                }
            };

            newWorld.creatures = newWorld.getCreatures();
            newWorld.alreadyExists = () => World.prototype.alreadyExists.call(newWorld, newWorld.getCreatures());

            var visualizer: IVisualizer = new Visualizer();
            expect(visualizer.toString(newWorld)).toBe('X, \n, , \n');
        });
    })

})();