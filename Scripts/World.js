var World = (function () {
    function World(x, y) {
        /// <summary>
        /// Build World of size x and y 
        /// </summary>
        this.x = x;
        this.y = y;
        this.creatures = [];
        // Fill the world with beasts
        // number of beasts is based on world dimension
        var count = Math.floor((x + y) * 6);
        // sanity check
        if (count > x * y) {
            count = x;
        }
        this.addCreatures(count);
    }
    World.prototype.getDimensions = function () {
        return [this.x, this.y];
    };
    World.prototype.getNextGeneration = function () {
        var _this = this;
        // surving creatures
        var nextCreatures = this.creatures.filter(function (c) { return _this.canLive(c); });
        // get empty neighbours and check which can become alive
        var emptyCells = [];
        this.creatures.forEach(function (c) {
            // prevents duplication of cells
            var nextCells = _this.getNeighbours(c)
                .filter(function (c) { return _this.alreadyExists(c, emptyCells); });
            emptyCells.concat(nextCells);
        });
        // check which of them can become alive
        var zombies = emptyCells.filter(function (c) { return _this.canLive(c); });
        nextCreatures = nextCreatures.concat(zombies);
        this.creatures = nextCreatures;
        return this;
    };
    World.prototype.addCreatures = function (n) {
        for (var i = 0; i < n; i++) {
            this.generateCreature();
        }
        console.log('creatures', this.creatures);
    };
    World.prototype.generateCreature = function () {
        var x = this.getRandomX();
        var y = this.getRandomY();
        if (this.alreadyExists([x, y]) == false) {
            this.creatures.push([x, y]);
        }
        else {
            this.generateCreature();
        }
    };
    World.prototype.getCreatures = function () {
        return this.creatures;
    };
    World.prototype.getRandomX = function () {
        return Math.floor(Math.random() * this.x) + 1;
    };
    World.prototype.getRandomY = function () {
        return Math.floor(Math.random() * this.y) + 1;
    };
    World.prototype.alreadyExists = function (creature, list) {
        if (list === void 0) { list = this.creatures; }
        var match = list.filter(function (c) { return c[0] == creature[0] && c[1] == creature[1]; });
        return match.length > 0;
    };
    World.prototype.isInWorld = function (creature) {
        return creature[0] <= this.x && creature[1] <= this.y;
    };
    World.prototype.canLive = function (creature) {
        var _this = this;
        var isLiving = this.alreadyExists(creature);
        // count neighbours
        var coordinates = this.getNeighbours(creature);
        var countLivingNeighbours = coordinates.filter(function (c) { return _this.alreadyExists(c); }).length;
        if (isLiving) {
            switch (countLivingNeighbours) {
                case 1:
                    // underpopulation
                    return false;
                    break;
                case 2:
                    // optimum, creature lives
                    return true;
                    break;
                case 3:
                    // optimum, creature lives
                    return true;
                    break;
                default:
                    // overcrowding
                    return false;
                    break;
            }
        }
        else if (countLivingNeighbours == 3) {
            // dead cell becomse alive
            return true;
        }
        return false;
    };
    World.prototype.getNeighbours = function (creature) {
        var coordinates = [
            [creature[0], creature[0] + 1],
            [creature[0] + 1, creature[0] + 1],
            [creature[0] + 1, creature[0]],
            [creature[0] + 1, creature[0] - 1],
            [creature[0], creature[0] - 1],
            [creature[0] - 1, creature[0] - 1],
            [creature[0] - 1, creature[0]],
            [creature[0] - 1, creature[0] + 1],
        ];
        return coordinates;
    };
    return World;
})();
module.exports = World;
//# sourceMappingURL=World.js.map