(function () {
    var World = require('./World');
    describe("World", function () {
        it("should create world with N creatures", function () {
            var newWorld = new World(2, 3);
            var creatures = newWorld.getCreatures();
            expect(creatures.length).toBe(2);
        });
        it("should create unique creatures", function () {
            var newWorld = new World(2, 3);
            var creatures = newWorld.getCreatures();
            // TODO
        });
    });
})();
//# sourceMappingURL=WorldSpec.js.map