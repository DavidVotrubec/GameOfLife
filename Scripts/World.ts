﻿interface IWorld {
    new (x: number, y: number);
    addCreatures(number);
    getCreatures(): Array<[number, number]>;
    getNextGeneration(): IWorld;
    getDimensions(): [number, number];
    alreadyExists(creature: [number, number]): boolean;
}

class World implements IWorld {
    constructor(private x: number, private y: number) {
        /// <summary>
        /// Build World of size x and y 
        /// </summary>

        // Fill the world with beasts
        // number of beasts is based on world dimension
        var count = Math.floor((x + y) * 6);

        // sanity check
        if (count > x * y) {
            count = x;
        }
        

        this.addCreatures(count);
    }

    private creatures = [];

    getDimensions() {
        return [this.x, this.y];
    }

    getNextGeneration(): World {
        // surving creatures
        var nextCreatures = this.creatures.filter((c) => this.canLive(c));

        // get empty neighbours and check which can become alive
        var emptyCells = [];

        this.creatures.forEach((c) => {
            // prevents duplication of cells
            let nextCells = this.getNeighbours(c)
                .filter((c: [number, number]) => this.alreadyExists(c, emptyCells));

            emptyCells.concat(nextCells);
        });

        // check which of them can become alive
        var zombies = emptyCells.filter(c => this.canLive(c));
        nextCreatures = nextCreatures.concat(zombies);

        this.creatures = nextCreatures;

        return this;
    }

    addCreatures(n: number) {
        for (var i = 0; i < n; i++) {
            this.generateCreature();            
        }

        console.log('creatures', this.creatures);
    }

    private generateCreature() {
        var x = this.getRandomX();
        var y = this.getRandomY();

        if (this.alreadyExists([x, y]) == false) {
            this.creatures.push([x, y]);
        }
        else {
            this.generateCreature();
        }
    }

    getCreatures() {
        return this.creatures;
    }

    private getRandomX() {
        return Math.floor(Math.random() * this.x) + 1;
    }

    private getRandomY() {
        return Math.floor(Math.random() * this.y) + 1;
    }

    alreadyExists(creature: [number, number], list = this.creatures) {
        var match = list.filter((c) => c[0] == creature[0] && c[1] == creature[1]);
        return match.length > 0;
    }

    private isInWorld(creature: [number, number]): boolean {
        return creature[0] <= this.x && creature[1] <= this.y;
    }

    private canLive(creature: [number, number]) {
        var isLiving = this.alreadyExists(creature);

        // count neighbours
        var coordinates = this.getNeighbours(creature);

        var countLivingNeighbours = coordinates.filter((c: [number, number]) => this.alreadyExists(c)).length;

        if (isLiving) {
            switch (countLivingNeighbours) {
                case 1:
                    // underpopulation
                    return false;
                    break;
                case 2:
                    // optimum, creature lives
                    return true;
                    break;
                case 3:
                    // optimum, creature lives
                    return true;
                    break;
                default:
                    // overcrowding
                    return false;
                    break;
            }
        }
        else if (countLivingNeighbours == 3) {
            // dead cell becomse alive
            return true;
        }

        return false;
    }

    private getNeighbours(creature: [number, number]) {
        var coordinates = [
            [creature[0], creature[0] + 1],
            [creature[0] + 1, creature[0] + 1],
            [creature[0] + 1, creature[0]],
            [creature[0] + 1, creature[0] - 1],
            [creature[0], creature[0] - 1],
            [creature[0] - 1, creature[0] - 1],
            [creature[0] - 1, creature[0]],
            [creature[0] - 1, creature[0] + 1],
        ];

        return coordinates;
    }
}

module.exports = World;