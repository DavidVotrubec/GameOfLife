﻿(function () {
    var World = require('./World');
    var Visualizer = require('./Visualizer');

    var instance: IWorld = new World(10, 10);
    var visualizer: IVisualizer = new Visualizer();
        
    setInterval(function () {
        var element = document.getElementById('visualizer');
        instance.getNextGeneration();
        var state = visualizer.toString(instance);
        element.textContent = state;
    }, 1000);

    //console.log('Visualizer', state);
})();
