var Visualizer = (function () {
    function Visualizer() {
    }
    Visualizer.prototype.toString = function (world) {
        // TODO: Should be tested with mocked world
        var dimensions = world.getDimensions();
        var creatures = world.getCreatures();
        var x = dimensions[0];
        var y = dimensions[1];
        var allCells = [];
        for (var i = 0; i < x; i++) {
            for (var j = 0; j < y; j++) {
                allCells.push([i, j]);
            }
        }
        allCells.forEach(function (c, index) {
            var isLiving = world.alreadyExists(c);
            if (isLiving) {
                allCells[index] = "X";
            }
            else {
                allCells[index] = " ";
            }
        });
        // split array into an array of array (per lines)
        var resultLines = []; //array of arrays
        for (var i = 0; i < x; i++) {
            var row = allCells.slice(0, x);
            resultLines.push(row);
            allCells = allCells.slice(x);
        }
        var result = '';
        resultLines.forEach(function (row) {
            result += row.toString() + '\n';
        });
        //return allCells.toString();
        return result;
    };
    return Visualizer;
})();
module.exports = Visualizer;
//# sourceMappingURL=Visualizer.js.map