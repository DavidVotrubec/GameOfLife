var World = (function () {
    function World(x, y) {
        /// <summary>
        /// Build World of size x and y 
        /// </summary>
        this.x = x;
        this.y = y;
        this.creatures = [];
        // Fill the world with beasts
        this.addCreatures(x + y);
    }
    World.prototype.getNext = function () {
        var world = new World(this.x, this.y);
    };
    World.prototype.addCreatures = function (n) {
        for (var i = 0; i < n; i++) {
            var x = this.getRandomX();
            var y = this.getRandomY();
            //TODO: Check that creatures does not exist
            this.creatures.push([x, y]);
        }
        console.log('test', this.creatures);
    };
    World.prototype.getCreatures = function () {
        return this.creatures;
    };
    World.prototype.getRandomX = function () {
        return Math.floor(Math.random() * this.x) + 1;
    };
    World.prototype.getRandomY = function () {
        return Math.floor(Math.random() * this.y) + 1;
    };
    World.prototype.isInWorld = function (creature) {
        return creature[0] <= this.x && creature[1] <= this.y;
    };
    return World;
})();
//# sourceMappingURL=World.js.map