(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function () {
    var World = require('./World');
    var Visualizer = require('./Visualizer');
    var instance = new World(10, 10);
    var visualizer = new Visualizer();
    setInterval(function () {
        var element = document.getElementById('visualizer');
        instance.getNextGeneration();
        var state = visualizer.toString(instance);
        element.textContent = state;
    }, 1000);
    //console.log('Visualizer', state);
})();

},{"./Visualizer":2,"./World":3}],2:[function(require,module,exports){
var Visualizer = (function () {
    function Visualizer() {
    }
    Visualizer.prototype.toString = function (world) {
        // TODO: Should be tested with mocked world
        var dimensions = world.getDimensions();
        var creatures = world.getCreatures();
        var x = dimensions[0];
        var y = dimensions[1];
        var allCells = [];
        for (var i = 0; i < x; i++) {
            for (var j = 0; j < y; j++) {
                allCells.push([i, j]);
            }
        }
        allCells.forEach(function (c, index) {
            var isLiving = world.alreadyExists(c);
            if (isLiving) {
                allCells[index] = "X";
            }
            else {
                allCells[index] = " ";
            }
        });
        // split array into an array of array (per lines)
        var resultLines = []; //array of arrays
        for (var i = 0; i < x; i++) {
            var row = allCells.slice(0, x);
            resultLines.push(row);
            allCells = allCells.slice(x);
        }
        var result = '';
        resultLines.forEach(function (row) {
            result += row.toString() + '\n';
        });
        //return allCells.toString();
        return result;
    };
    return Visualizer;
})();
module.exports = Visualizer;

},{}],3:[function(require,module,exports){
var World = (function () {
    function World(x, y) {
        /// <summary>
        /// Build World of size x and y 
        /// </summary>
        this.x = x;
        this.y = y;
        this.creatures = [];
        // Fill the world with beasts
        // number of beasts is based on world dimension
        var count = Math.floor((x + y) * 6);
        // sanity check
        if (count > x * y) {
            count = x;
        }
        this.addCreatures(count);
    }
    World.prototype.getDimensions = function () {
        return [this.x, this.y];
    };
    World.prototype.getNextGeneration = function () {
        var _this = this;
        // surving creatures
        var nextCreatures = this.creatures.filter(function (c) { return _this.canLive(c); });
        // get empty neighbours and check which can become alive
        var emptyCells = [];
        this.creatures.forEach(function (c) {
            // prevents duplication of cells
            var nextCells = _this.getNeighbours(c)
                .filter(function (c) { return _this.alreadyExists(c, emptyCells); });
            emptyCells.concat(nextCells);
        });
        // check which of them can become alive
        var zombies = emptyCells.filter(function (c) { return _this.canLive(c); });
        nextCreatures = nextCreatures.concat(zombies);
        this.creatures = nextCreatures;
        return this;
    };
    World.prototype.addCreatures = function (n) {
        for (var i = 0; i < n; i++) {
            this.generateCreature();
        }
        console.log('creatures', this.creatures);
    };
    World.prototype.generateCreature = function () {
        var x = this.getRandomX();
        var y = this.getRandomY();
        if (this.alreadyExists([x, y]) == false) {
            this.creatures.push([x, y]);
        }
        else {
            this.generateCreature();
        }
    };
    World.prototype.getCreatures = function () {
        return this.creatures;
    };
    World.prototype.getRandomX = function () {
        return Math.floor(Math.random() * this.x) + 1;
    };
    World.prototype.getRandomY = function () {
        return Math.floor(Math.random() * this.y) + 1;
    };
    World.prototype.alreadyExists = function (creature, list) {
        if (list === void 0) { list = this.creatures; }
        var match = list.filter(function (c) { return c[0] == creature[0] && c[1] == creature[1]; });
        return match.length > 0;
    };
    World.prototype.isInWorld = function (creature) {
        return creature[0] <= this.x && creature[1] <= this.y;
    };
    World.prototype.canLive = function (creature) {
        var _this = this;
        var isLiving = this.alreadyExists(creature);
        // count neighbours
        var coordinates = this.getNeighbours(creature);
        var countLivingNeighbours = coordinates.filter(function (c) { return _this.alreadyExists(c); }).length;
        if (isLiving) {
            switch (countLivingNeighbours) {
                case 1:
                    // underpopulation
                    return false;
                    break;
                case 2:
                    // optimum, creature lives
                    return true;
                    break;
                case 3:
                    // optimum, creature lives
                    return true;
                    break;
                default:
                    // overcrowding
                    return false;
                    break;
            }
        }
        else if (countLivingNeighbours == 3) {
            // dead cell becomse alive
            return true;
        }
        return false;
    };
    World.prototype.getNeighbours = function (creature) {
        var coordinates = [
            [creature[0], creature[0] + 1],
            [creature[0] + 1, creature[0] + 1],
            [creature[0] + 1, creature[0]],
            [creature[0] + 1, creature[0] - 1],
            [creature[0], creature[0] - 1],
            [creature[0] - 1, creature[0] - 1],
            [creature[0] - 1, creature[0]],
            [creature[0] - 1, creature[0] + 1],
        ];
        return coordinates;
    };
    return World;
})();
module.exports = World;

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL1VzZXJzL0RhdmlkL0FwcERhdGEvUm9hbWluZy9ucG0vbm9kZV9tb2R1bGVzL3dhdGNoaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJTY3JpcHRzL0FwcC5qcyIsIlNjcmlwdHMvVmlzdWFsaXplci5qcyIsIlNjcmlwdHMvV29ybGQuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN6Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIihmdW5jdGlvbiAoKSB7XHJcbiAgICB2YXIgV29ybGQgPSByZXF1aXJlKCcuL1dvcmxkJyk7XHJcbiAgICB2YXIgVmlzdWFsaXplciA9IHJlcXVpcmUoJy4vVmlzdWFsaXplcicpO1xyXG4gICAgdmFyIGluc3RhbmNlID0gbmV3IFdvcmxkKDEwLCAxMCk7XHJcbiAgICB2YXIgdmlzdWFsaXplciA9IG5ldyBWaXN1YWxpemVyKCk7XHJcbiAgICBzZXRJbnRlcnZhbChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIGVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgndmlzdWFsaXplcicpO1xyXG4gICAgICAgIGluc3RhbmNlLmdldE5leHRHZW5lcmF0aW9uKCk7XHJcbiAgICAgICAgdmFyIHN0YXRlID0gdmlzdWFsaXplci50b1N0cmluZyhpbnN0YW5jZSk7XHJcbiAgICAgICAgZWxlbWVudC50ZXh0Q29udGVudCA9IHN0YXRlO1xyXG4gICAgfSwgMTAwMCk7XHJcbiAgICAvL2NvbnNvbGUubG9nKCdWaXN1YWxpemVyJywgc3RhdGUpO1xyXG59KSgpO1xyXG4vLyMgc291cmNlTWFwcGluZ1VSTD1BcHAuanMubWFwIiwidmFyIFZpc3VhbGl6ZXIgPSAoZnVuY3Rpb24gKCkge1xyXG4gICAgZnVuY3Rpb24gVmlzdWFsaXplcigpIHtcclxuICAgIH1cclxuICAgIFZpc3VhbGl6ZXIucHJvdG90eXBlLnRvU3RyaW5nID0gZnVuY3Rpb24gKHdvcmxkKSB7XHJcbiAgICAgICAgLy8gVE9ETzogU2hvdWxkIGJlIHRlc3RlZCB3aXRoIG1vY2tlZCB3b3JsZFxyXG4gICAgICAgIHZhciBkaW1lbnNpb25zID0gd29ybGQuZ2V0RGltZW5zaW9ucygpO1xyXG4gICAgICAgIHZhciBjcmVhdHVyZXMgPSB3b3JsZC5nZXRDcmVhdHVyZXMoKTtcclxuICAgICAgICB2YXIgeCA9IGRpbWVuc2lvbnNbMF07XHJcbiAgICAgICAgdmFyIHkgPSBkaW1lbnNpb25zWzFdO1xyXG4gICAgICAgIHZhciBhbGxDZWxscyA9IFtdO1xyXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgeDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGZvciAodmFyIGogPSAwOyBqIDwgeTsgaisrKSB7XHJcbiAgICAgICAgICAgICAgICBhbGxDZWxscy5wdXNoKFtpLCBqXSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgYWxsQ2VsbHMuZm9yRWFjaChmdW5jdGlvbiAoYywgaW5kZXgpIHtcclxuICAgICAgICAgICAgdmFyIGlzTGl2aW5nID0gd29ybGQuYWxyZWFkeUV4aXN0cyhjKTtcclxuICAgICAgICAgICAgaWYgKGlzTGl2aW5nKSB7XHJcbiAgICAgICAgICAgICAgICBhbGxDZWxsc1tpbmRleF0gPSBcIlhcIjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGFsbENlbGxzW2luZGV4XSA9IFwiIFwiO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgLy8gc3BsaXQgYXJyYXkgaW50byBhbiBhcnJheSBvZiBhcnJheSAocGVyIGxpbmVzKVxyXG4gICAgICAgIHZhciByZXN1bHRMaW5lcyA9IFtdOyAvL2FycmF5IG9mIGFycmF5c1xyXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgeDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHZhciByb3cgPSBhbGxDZWxscy5zbGljZSgwLCB4KTtcclxuICAgICAgICAgICAgcmVzdWx0TGluZXMucHVzaChyb3cpO1xyXG4gICAgICAgICAgICBhbGxDZWxscyA9IGFsbENlbGxzLnNsaWNlKHgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB2YXIgcmVzdWx0ID0gJyc7XHJcbiAgICAgICAgcmVzdWx0TGluZXMuZm9yRWFjaChmdW5jdGlvbiAocm93KSB7XHJcbiAgICAgICAgICAgIHJlc3VsdCArPSByb3cudG9TdHJpbmcoKSArICdcXG4nO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIC8vcmV0dXJuIGFsbENlbGxzLnRvU3RyaW5nKCk7XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH07XHJcbiAgICByZXR1cm4gVmlzdWFsaXplcjtcclxufSkoKTtcclxubW9kdWxlLmV4cG9ydHMgPSBWaXN1YWxpemVyO1xyXG4vLyMgc291cmNlTWFwcGluZ1VSTD1WaXN1YWxpemVyLmpzLm1hcCIsInZhciBXb3JsZCA9IChmdW5jdGlvbiAoKSB7XHJcbiAgICBmdW5jdGlvbiBXb3JsZCh4LCB5KSB7XHJcbiAgICAgICAgLy8vIDxzdW1tYXJ5PlxyXG4gICAgICAgIC8vLyBCdWlsZCBXb3JsZCBvZiBzaXplIHggYW5kIHkgXHJcbiAgICAgICAgLy8vIDwvc3VtbWFyeT5cclxuICAgICAgICB0aGlzLnggPSB4O1xyXG4gICAgICAgIHRoaXMueSA9IHk7XHJcbiAgICAgICAgdGhpcy5jcmVhdHVyZXMgPSBbXTtcclxuICAgICAgICAvLyBGaWxsIHRoZSB3b3JsZCB3aXRoIGJlYXN0c1xyXG4gICAgICAgIC8vIG51bWJlciBvZiBiZWFzdHMgaXMgYmFzZWQgb24gd29ybGQgZGltZW5zaW9uXHJcbiAgICAgICAgdmFyIGNvdW50ID0gTWF0aC5mbG9vcigoeCArIHkpICogNik7XHJcbiAgICAgICAgLy8gc2FuaXR5IGNoZWNrXHJcbiAgICAgICAgaWYgKGNvdW50ID4geCAqIHkpIHtcclxuICAgICAgICAgICAgY291bnQgPSB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmFkZENyZWF0dXJlcyhjb3VudCk7XHJcbiAgICB9XHJcbiAgICBXb3JsZC5wcm90b3R5cGUuZ2V0RGltZW5zaW9ucyA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICByZXR1cm4gW3RoaXMueCwgdGhpcy55XTtcclxuICAgIH07XHJcbiAgICBXb3JsZC5wcm90b3R5cGUuZ2V0TmV4dEdlbmVyYXRpb24gPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuICAgICAgICAvLyBzdXJ2aW5nIGNyZWF0dXJlc1xyXG4gICAgICAgIHZhciBuZXh0Q3JlYXR1cmVzID0gdGhpcy5jcmVhdHVyZXMuZmlsdGVyKGZ1bmN0aW9uIChjKSB7IHJldHVybiBfdGhpcy5jYW5MaXZlKGMpOyB9KTtcclxuICAgICAgICAvLyBnZXQgZW1wdHkgbmVpZ2hib3VycyBhbmQgY2hlY2sgd2hpY2ggY2FuIGJlY29tZSBhbGl2ZVxyXG4gICAgICAgIHZhciBlbXB0eUNlbGxzID0gW107XHJcbiAgICAgICAgdGhpcy5jcmVhdHVyZXMuZm9yRWFjaChmdW5jdGlvbiAoYykge1xyXG4gICAgICAgICAgICAvLyBwcmV2ZW50cyBkdXBsaWNhdGlvbiBvZiBjZWxsc1xyXG4gICAgICAgICAgICB2YXIgbmV4dENlbGxzID0gX3RoaXMuZ2V0TmVpZ2hib3VycyhjKVxyXG4gICAgICAgICAgICAgICAgLmZpbHRlcihmdW5jdGlvbiAoYykgeyByZXR1cm4gX3RoaXMuYWxyZWFkeUV4aXN0cyhjLCBlbXB0eUNlbGxzKTsgfSk7XHJcbiAgICAgICAgICAgIGVtcHR5Q2VsbHMuY29uY2F0KG5leHRDZWxscyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgLy8gY2hlY2sgd2hpY2ggb2YgdGhlbSBjYW4gYmVjb21lIGFsaXZlXHJcbiAgICAgICAgdmFyIHpvbWJpZXMgPSBlbXB0eUNlbGxzLmZpbHRlcihmdW5jdGlvbiAoYykgeyByZXR1cm4gX3RoaXMuY2FuTGl2ZShjKTsgfSk7XHJcbiAgICAgICAgbmV4dENyZWF0dXJlcyA9IG5leHRDcmVhdHVyZXMuY29uY2F0KHpvbWJpZXMpO1xyXG4gICAgICAgIHRoaXMuY3JlYXR1cmVzID0gbmV4dENyZWF0dXJlcztcclxuICAgICAgICByZXR1cm4gdGhpcztcclxuICAgIH07XHJcbiAgICBXb3JsZC5wcm90b3R5cGUuYWRkQ3JlYXR1cmVzID0gZnVuY3Rpb24gKG4pIHtcclxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IG47IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLmdlbmVyYXRlQ3JlYXR1cmUoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc29sZS5sb2coJ2NyZWF0dXJlcycsIHRoaXMuY3JlYXR1cmVzKTtcclxuICAgIH07XHJcbiAgICBXb3JsZC5wcm90b3R5cGUuZ2VuZXJhdGVDcmVhdHVyZSA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgeCA9IHRoaXMuZ2V0UmFuZG9tWCgpO1xyXG4gICAgICAgIHZhciB5ID0gdGhpcy5nZXRSYW5kb21ZKCk7XHJcbiAgICAgICAgaWYgKHRoaXMuYWxyZWFkeUV4aXN0cyhbeCwgeV0pID09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY3JlYXR1cmVzLnB1c2goW3gsIHldKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuZ2VuZXJhdGVDcmVhdHVyZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcbiAgICBXb3JsZC5wcm90b3R5cGUuZ2V0Q3JlYXR1cmVzID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNyZWF0dXJlcztcclxuICAgIH07XHJcbiAgICBXb3JsZC5wcm90b3R5cGUuZ2V0UmFuZG9tWCA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICByZXR1cm4gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogdGhpcy54KSArIDE7XHJcbiAgICB9O1xyXG4gICAgV29ybGQucHJvdG90eXBlLmdldFJhbmRvbVkgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgcmV0dXJuIE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIHRoaXMueSkgKyAxO1xyXG4gICAgfTtcclxuICAgIFdvcmxkLnByb3RvdHlwZS5hbHJlYWR5RXhpc3RzID0gZnVuY3Rpb24gKGNyZWF0dXJlLCBsaXN0KSB7XHJcbiAgICAgICAgaWYgKGxpc3QgPT09IHZvaWQgMCkgeyBsaXN0ID0gdGhpcy5jcmVhdHVyZXM7IH1cclxuICAgICAgICB2YXIgbWF0Y2ggPSBsaXN0LmZpbHRlcihmdW5jdGlvbiAoYykgeyByZXR1cm4gY1swXSA9PSBjcmVhdHVyZVswXSAmJiBjWzFdID09IGNyZWF0dXJlWzFdOyB9KTtcclxuICAgICAgICByZXR1cm4gbWF0Y2gubGVuZ3RoID4gMDtcclxuICAgIH07XHJcbiAgICBXb3JsZC5wcm90b3R5cGUuaXNJbldvcmxkID0gZnVuY3Rpb24gKGNyZWF0dXJlKSB7XHJcbiAgICAgICAgcmV0dXJuIGNyZWF0dXJlWzBdIDw9IHRoaXMueCAmJiBjcmVhdHVyZVsxXSA8PSB0aGlzLnk7XHJcbiAgICB9O1xyXG4gICAgV29ybGQucHJvdG90eXBlLmNhbkxpdmUgPSBmdW5jdGlvbiAoY3JlYXR1cmUpIHtcclxuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG4gICAgICAgIHZhciBpc0xpdmluZyA9IHRoaXMuYWxyZWFkeUV4aXN0cyhjcmVhdHVyZSk7XHJcbiAgICAgICAgLy8gY291bnQgbmVpZ2hib3Vyc1xyXG4gICAgICAgIHZhciBjb29yZGluYXRlcyA9IHRoaXMuZ2V0TmVpZ2hib3VycyhjcmVhdHVyZSk7XHJcbiAgICAgICAgdmFyIGNvdW50TGl2aW5nTmVpZ2hib3VycyA9IGNvb3JkaW5hdGVzLmZpbHRlcihmdW5jdGlvbiAoYykgeyByZXR1cm4gX3RoaXMuYWxyZWFkeUV4aXN0cyhjKTsgfSkubGVuZ3RoO1xyXG4gICAgICAgIGlmIChpc0xpdmluZykge1xyXG4gICAgICAgICAgICBzd2l0Y2ggKGNvdW50TGl2aW5nTmVpZ2hib3Vycykge1xyXG4gICAgICAgICAgICAgICAgY2FzZSAxOlxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHVuZGVycG9wdWxhdGlvblxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgMjpcclxuICAgICAgICAgICAgICAgICAgICAvLyBvcHRpbXVtLCBjcmVhdHVyZSBsaXZlc1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSAzOlxyXG4gICAgICAgICAgICAgICAgICAgIC8vIG9wdGltdW0sIGNyZWF0dXJlIGxpdmVzXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgICAgIC8vIG92ZXJjcm93ZGluZ1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIGlmIChjb3VudExpdmluZ05laWdoYm91cnMgPT0gMykge1xyXG4gICAgICAgICAgICAvLyBkZWFkIGNlbGwgYmVjb21zZSBhbGl2ZVxyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfTtcclxuICAgIFdvcmxkLnByb3RvdHlwZS5nZXROZWlnaGJvdXJzID0gZnVuY3Rpb24gKGNyZWF0dXJlKSB7XHJcbiAgICAgICAgdmFyIGNvb3JkaW5hdGVzID0gW1xyXG4gICAgICAgICAgICBbY3JlYXR1cmVbMF0sIGNyZWF0dXJlWzBdICsgMV0sXHJcbiAgICAgICAgICAgIFtjcmVhdHVyZVswXSArIDEsIGNyZWF0dXJlWzBdICsgMV0sXHJcbiAgICAgICAgICAgIFtjcmVhdHVyZVswXSArIDEsIGNyZWF0dXJlWzBdXSxcclxuICAgICAgICAgICAgW2NyZWF0dXJlWzBdICsgMSwgY3JlYXR1cmVbMF0gLSAxXSxcclxuICAgICAgICAgICAgW2NyZWF0dXJlWzBdLCBjcmVhdHVyZVswXSAtIDFdLFxyXG4gICAgICAgICAgICBbY3JlYXR1cmVbMF0gLSAxLCBjcmVhdHVyZVswXSAtIDFdLFxyXG4gICAgICAgICAgICBbY3JlYXR1cmVbMF0gLSAxLCBjcmVhdHVyZVswXV0sXHJcbiAgICAgICAgICAgIFtjcmVhdHVyZVswXSAtIDEsIGNyZWF0dXJlWzBdICsgMV0sXHJcbiAgICAgICAgXTtcclxuICAgICAgICByZXR1cm4gY29vcmRpbmF0ZXM7XHJcbiAgICB9O1xyXG4gICAgcmV0dXJuIFdvcmxkO1xyXG59KSgpO1xyXG5tb2R1bGUuZXhwb3J0cyA9IFdvcmxkO1xyXG4vLyMgc291cmNlTWFwcGluZ1VSTD1Xb3JsZC5qcy5tYXAiXX0=
